#prog extracts email id, domain, time

import re

#input given directly or can be taken through standard input

str = "From abc.xyz@pqr.com Mon Dec 29 01:12:15 2016"

email = re.search('[\w\.-]+@[\w\.-]+', str).group(0)
print email

domain = re.search('@[a-z]+', str).group(0)
print domain

time = re.search('[\w\.-]+:[\w\.-]+:[\w\.-]+', str).group(0)
print time