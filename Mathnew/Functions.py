class Functions:
    def __init__(self, a = None , b = None):
        if a != None:  
            self.a = a
        if b != None:
            self.b = b
    def sqroot(self, a = None):
        if a != None:
            return a ** 0.5
        else:
            return self.a ** 0.5
    def addition(self, a = None, b = None):
        if a != None and b != None:
            return a + b
        else:
            return self.a + self.b
    def subtraction(self, a = None, b = None):
        if a != None and b != None:
            return a - b
        else:
            return self.a - self.b
    def multiplication(self, a = None, b = None):
        if a != None and b != None:
            return a * b
        else:
            return self.a * self.b
    def division(self, a = None, b = None):
        if a != None and b != None:
            return a / b
        else:
            return self.a / self.b