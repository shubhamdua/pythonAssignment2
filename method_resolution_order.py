# Given is the code implementing four classes
# The method resolution order is exlained after the code in the comments

class First:
    def method1(self):
        print "First class"
        
#class Second inherits from first
class Second(First):
    def method1(self):
        print "Second class method"
        
#class Third inherits from First
class Third(First):
    def method1(self):
        print "Third class method"
        
#class Fourth inherits from Second and third
class Fourth(Second, Third):
    def method1(self):
        print "Fourth class method"

d = Fourth()
d.method1()

# Now the method resolution order when we call method1 from object od class Fourth will be:
#  Fourth, Second, Third, First
# if we define class Fourth a s:
# class Fourth(Third, Second)
# then the order will change to: Fourth, Third, Second, First